
from os.path import join, dirname
from os import getenv
from dotenv import load_dotenv

__all__ = ['Config']


env_path = join(dirname(__file__), '.env')
load_dotenv(env_path)


class Log:
    log_lvl = None
    tool_bar = False

    def log_is(self):
        return __class__.__name__

    def __init__(self):
        self._logging = {
            'version': 1,
            'formatters': {
                'detail': {
                    'format': '{asctime} {levelname:5.5} [{process}] [{name}] [{threadName}] {message}',
                    'style': '{'
                },
                'simple': {}
            },
            'handlers': {
                'console': {
                    'class': 'logging.StreamHandler',
                    'formatter': 'detail',
                    'stream': 'ext://sys.stdout'
                }
            },
            'root': {
                'level': self.log_lvl,
                'handlers': ['console']
            },
            'loggers': {
                'asyncio': {},
                'aiohttp': {},
                'gunicorn': {},
                'web': {}
            }
        }

    @property
    def logging(self):
        return self._logging


class Dev(Log):
    log_lvl = 'DEBUG'
    loop_debug = True
    tool_bar = False


class Web(Log):
    log_lvl = 'WARNING'
    loop_debug = False
    tool_bar = True


class Config:
    def __new__(cls):
        if getenv('ENV') == 'DEVELOPMENT':
            _name = Dev
        else:
            _name = Web
        return _name()

