
/*small status messages*/
function show_message(id, text)
{
    let element = document.getElementById(id);
    element.innerHTML = text;
    element.classList.add('fade');
    setTimeout(() => {
        element.classList.remove('fade');
        element.innerHTML='';
        },3000);
}

/*sing request*/
function signin(form)
{
    fetch('/sign', {
        method: 'post',
        body: new FormData(form),
    }).then((response) => {
        return response.json();
    }).then(data => {
        if (data.status){
            window.location.href = '/admin'
        }else{
            show_message('message','Incorrect login or password');
        }
    }).catch(err => {
        console.error(err)
    });

}

/*feedback message request*/
function mail(form)
{
    fetch("/contacts", {
        method: 'post',
        redirect:'follow',
        body: new FormData(form),
    }).then((response) => {
        if (response.ok) {
            form.reset();
            show_message('message', 'Feedback has been send');
        }
    });
}

/*feedback message request*/
function mail2(form)
{
    fetch("/contacts", {
        method: 'post',
        redirect:'follow',
        body: new FormData(form),
    }).then((response) => {
        if (response.ok) {
            show_message('message', 'Feedback has been send');
        }
    });
}

/*settings update request*/
function settings(form)
{
    console.log(new FormData(form));
    fetch("/admin/settings", {
        method: 'post',
        redirect:'follow',
        body: new FormData(form),
    }).then((response) => {
        if (response.ok) {
            show_message('message', 'Settings has been update');
            form.set('st-key','sdvsdvsdvwevwe');
        }
    });
}