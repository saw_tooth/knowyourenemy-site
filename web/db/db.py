import asyncpg
from asyncpg import UnknownPostgresError
import json
import re
import hashlib
import ssl

async def open_database(app):
    import os
    url = os.getenv('DATABASE_URL')
    max_conn = os.getenv('MAX_DB_POOL')
    ssl_object = ssl.create_default_context()
    ssl_object.check_hostname = False
    ssl_object.verify_mode = ssl.CERT_NONE
    app['db'] = await asyncpg.create_pool(dsn=url, min_size=1,ssl=ssl_object, max_size=int(max_conn))


async def close_db(app):
    await app['db'].close()


async def setup_db(app):
    await open_database(app)
    app.on_shutdown.append(close_db)


class Pool:
    def __init__(self, request):
        self.conn = request.app['db']


class Postgress(Pool):

    async def versions(self):
        return await self.conn.fetch('SELECT version, link, c_date FROM links')

    async def changelogs(self):
        return await self.conn.fetch("SELECT version, array(SELECT record FROM changelogs where id = rec.id) as changes FROM links rec")

    async def faqs(self):
        return await self.conn.fetch("SELECT question, answer FROM faq")

    async def alldata(self):
        return await self.conn.fetch("SELECT version, link, c_date as date, array(SELECT record FROM changelogs where id = rec.id) as changes FROM links rec")

    async def statistics(self):
        return await self.conn.fetchrow("SELECT sum(counter) as summ, array(SELECT (version, counter) FROM links) as counters FROM links")


class Faq(Pool):
    scheme = [
        'question',
        'answer'
    ]

    def _valid(self, data):
        return all(k in data for k in self.scheme)

    async def get(self):
        return await self.conn.fetch("SELECT question, answer FROM faq")

    async def set(self, params):
        if self._valid(params):
            quest = params['question']
            answ = params['answer']
            return await self.conn.fetch("INSERT INTO feedback (question, answer) VALUES ($1, $2) RETURNING id", quest, answ)
        else:
            return False

    async def update(self, params, q_id):
        if self._valid(params):
            quest = params['question']
            answ = params['answer']
            return await self.conn.fetch("UPDATE feedback set (question, answer) = ($1, $2) where id=$3 RETURNING id", quest, answ, q_id)
        else:
            return False


class Feedback(Pool):
    scheme = [
            'letter',
            'email'
        ]

    def _valid(self, data):
        return all(k in data for k in self.scheme) and re.search("[@.]", data['email']) is not None

    async def new(self, data):
        if self._valid(data):
            email = data['email']
            text = data['letter']
            return await self.conn.fetchval("INSERT INTO feedback (address, incoming) VALUES($1, $2) RETURNING id", email, text)
        else:
            return False

    async def messages(self):
        return await self.conn.fetch('SELECT * FROM feedback')

    async def get_message(self, m_id):
        return await self.conn.fetchrow('SELECT * FROM feedback WHERE id=($1)', m_id)

    async def mark_readed(self, m_id):
        return await self.conn.fetchval('update feedback set setnew=False where id=($1) RETURNING id', m_id)

    async def set_answer(self, m_id, text):
        return await self.conn.fetchval('update feedback set (outcomimg, isanswered) = ($1, True) where id=$2 RETURNING id', text, m_id)


class Settings(Pool):

    scheme = [
        "e-mail",
        "e-pwd",
        "e-server",
        "e-port",
        "st-server",
        "st-key",
        "st-path"
    ]

    def _valid(self, value):
        return all(k in value for k in self.scheme)

    async def set(self, value):
        if self._valid(value):
            record = json.dumps({x: value[x] for x in self.scheme})
            await self.conn.execute("UPDATE settings SET keys=($1)", record)
            return True
        else:
            return False

    async def get(self):
        row = await self.conn.fetchrow("SELECT (keys) FROM settings")
        return json.loads(row['keys'])


class Users:

    def __init__(self, db):
        self.conn = db

    scheme = [
        "login",
        "password"
    ]

    def _valid(self, value):
        return all(k in value for k in self.scheme)

    async def has_user(self, params):
        result = None
        if self._valid(params):
            result = await self.conn.fetchrow("select * from users where name=$1 and password=encode(digest($2, 'sha256'), 'hex')",
                                              params['login'], params['password'])
        if result:
            return result['name']
        else:
            return None

    async def add_user(self, name, pwd):
        await self.conn.execute("INSERT INTO users (name, password) VALUES ($1, encode(digest($2, 'sha256'), 'hex'))",
                                name, pwd)

    async def present(self, identity):
        return await self.conn.fetchrow("SELECT True FROM users WHERE name = $1", identity)


