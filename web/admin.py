from aiohttp.web import Response, View, HTTPFound,RouteTableDef
from aiohttp_security import check_authorized, remember, forget
from web.db.db import Postgress, Settings, Users, Feedback

import aiohttp_jinja2
import json

routes = RouteTableDef()


@routes.get('/admin')
@aiohttp_jinja2.template("admin/admin.jinja2")
async def main(request):
    await check_authorized(request)
    data = Postgress(request)
    return {"data": await data.alldata()}


@routes.view('/admin/builds')
class BuildsView(View):

    @aiohttp_jinja2.template("admin/builds.jinja2")
    async def get(self):
        db = Postgress(self.request)
        return {'data': await db.statistics()}

    @aiohttp_jinja2.template("admin/addbuild.jinja2")
    async def post(self):
        return {}


@routes.view('/admin/changelog')
class BuildsView(View):

    @aiohttp_jinja2.template("admin/change.jinja2")
    async def get(self):
        db = Postgress(self.request)
        return {'data': await db.statistics()}

    @aiohttp_jinja2.template("admin/addbuild.jinja2")
    async def post(self):
        return {}


@routes.view('/admin/settings')
class SettingsView(View):

    @aiohttp_jinja2.template("admin/settings.jinja2")
    async def get(self):
        await check_authorized(self.request)
        data = Settings(self.request)
        return {'keys': await data.get()}

    #@aiohttp_jinja2.template("admin/settings.jinja2")
    async def post(self):
        await check_authorized(self.request)
        data = Settings(self.request)
        param = await self.request.post()
        if await data.set(param):
            return Response()#{'keys': await data.get()}


@routes.view('/admin/inbox')
class MailBoxView(View):

    @aiohttp_jinja2.template("admin/inbox.jinja2")
    async def get(self):
        data = Feedback(self.request)
        return {"messages": await data.messages()}

    async def post(self):
        data = Feedback(self.request)
        return {"messages": await data.new()}


@routes.view('/sign')
class SignView(View):

    @aiohttp_jinja2.template("admin/signin.jinja2")
    async def get(self):
        return {}

    async def post(self):
        db = Users(self.request.app['db'])
        user = await db.has_user(await self.request.post())
        response = Response()
        if user:
            status = True
            await remember(self.request, response, user)
        else:
            status = False
        response.text = json.dumps({'status': status})
        return response


@routes.get('/logout')
async def logout(request):
    redirect_url = HTTPFound('/')
    await forget(request, redirect_url)
    raise redirect_url


def setup_admin_routes(app):
    app.router.add_routes(routes)
