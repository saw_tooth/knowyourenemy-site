from aiohttp_session import setup as setup_session
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from aiohttp_security import SessionIdentityPolicy, setup as setup_security
from aiohttp_security.abc import AbstractAuthorizationPolicy
from web.db.db import Users

import logging
log = logging.getLogger(__name__)


class UserAuth(AbstractAuthorizationPolicy):

    def __init__(self, db):
        self.db = db

    async def authorized_userid(self, identity):
        """Retrieve authorized user id.
        Return the user_id of the user identified by the identity
        or 'None' if no user exists related to the identity.
        """
        print(identity)
        sql = Users(self.db)
        print(self.db)
        retu = await sql.present(identity)
        print(retu)
        return retu

    async def permits(self, identity, permission, context=None):
        """Check user permissions.
        Return True if the identity is allowed the permission
        in the current context, else return False.
        """
        return True


async def setup_auth(app):
    setup_session(app, EncryptedCookieStorage(max_age=36000, secret_key=b'a'*32, cookie_name='auth_key'))
    setup_security(app, SessionIdentityPolicy(), UserAuth(app['db']))
