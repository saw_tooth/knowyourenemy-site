from aiohttp_jinja2 import template
from aiohttp.web import RouteTableDef, Response

from web.db.db import Postgress, Settings, Feedback, Faq


routes = RouteTableDef()


@routes.get('/')
@template("public/home.jinja2")
async def home(request):
    return {}


@routes.get('/about')
@template("public/about.jinja2")
async def about(request):
    return {}


@routes.get('/features')
@template("public/features.jinja2")
async def features(request):
    return {}


@routes.get('/changelog')
@template("public/changelog.jinja2")
async def changelog(request):
    data = Postgress(request)
    return {'record': await data.changelogs()}


@routes.get('/download')
@template("public/download.jinja2")
async def download(request):
    data2 = Postgress(request)
    return {'versions': await data2.versions()}


@routes.get('/faq')
@template("public/faq.jinja2")
async def faq(request):
    data = Faq(request)
    return {'record': await data.get()}


@routes.get('/contacts')
@template("public/contacts.jinja2")
async def contacts(request):
    return {}


@routes.post('/contacts')
async def send_mail(request):
    sql = Feedback(request)
    result = await sql.new(await request.post())
    if result:
        return Response()
    else:
        return Response(status=400)


def setup_public_routes(app):
    app.router.add_routes(routes)
