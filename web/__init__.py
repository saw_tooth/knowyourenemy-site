
import logging
import logging.config
import asyncio

from aiohttp import web
from web.error import setup_error_handlers
from web.public import setup_public_routes
from web.admin import setup_admin_routes

from web.utils.settings import *
from config import conf
from web.auth import setup_auth
from web.db.db import setup_db


here_folder = os.path.dirname(os.path.abspath(__file__))
log = logging.getLogger(__name__)


def build_app():

    settings = conf.Config()

    logging.config.dictConfig(settings.logging)

    loop = asyncio.get_event_loop()
    loop.set_debug(settings.loop_debug)

    application = web.Application(loop=loop)

    application.on_startup.append(setup_db)
    application.on_startup.append(setup_auth)
    setup_static(application)
    setup_template(application)
    setup_error_handlers(application)
    setup_admin_routes(application)
    setup_public_routes(application)

    return application


main = build_app()
