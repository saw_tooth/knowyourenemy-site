import os
import aiohttp_jinja2
import jinja2


def setup_static(app):
    st = os.path.join(os.path.dirname(__file__), '../../static')
    app.router.add_static('/static', st)


def setup_template(app):
    template_dir = os.path.join(os.path.dirname(__file__), '../templates')
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(template_dir), trim_blocks=True, lstrip_blocks=True)
