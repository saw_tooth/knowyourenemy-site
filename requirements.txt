aiohttp_security
aiohttp-jinja2==1.0.0
aiohttp>=3.0.1
asyncpg>=0.18.0
cryptography>=2.4.0
gunicorn==19.9.0
aiohttp-session[security]>=2.7.0
python-dotenv

